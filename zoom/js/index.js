window.addEventListener('DOMContentLoaded', function(event) {
  console.log('DOM fully loaded and parsed');
  websdkready();
});
var meetingNumber="89806560150"
var meetingPassword="731003"
var userName=localStorage.getItem('userName')||'test'
var meetingEmail="test@gmail.com"
function websdkready() {
  var testTool = window.testTool;
  if (testTool.isMobileDevice()) {
    vConsole = new VConsole();
  }
  console.log("checkSystemRequirements");
  console.log(JSON.stringify(ZoomMtg.checkSystemRequirements()));

  // it's option if you want to change the WebSDK dependency link resources. setZoomJSLib must be run at first
  // if (!china) ZoomMtg.setZoomJSLib('https://source.zoom.us/2.8.0/lib', '/av'); // CDN version default
  // else ZoomMtg.setZoomJSLib('https://jssdk.zoomus.cn/2.8.0/lib', '/av'); // china cdn option
  // ZoomMtg.setZoomJSLib('http://localhost:9999/node_modules/@zoomus/websdk/dist/lib', '/av'); // Local version default, Angular Project change to use cdn version
  ZoomMtg.preLoadWasm(); // pre download wasm file to save time.

  var SDK_KEY = "R6bfm5KrDOmVZJ8QlCI2SuqnNbVQ57lsMBsW";
  /**
   * NEVER PUT YOUR ACTUAL SDK SECRET IN CLIENT SIDE CODE, THIS IS JUST FOR QUICK PROTOTYPING
   * The below generateSignature should be done server side as not to expose your SDK SECRET in public
   * You can find an eaxmple in here: https://marketplace.zoom.us/docs/sdk/native-sdks/web/essential/signature
   */
  var SDK_SECRET = "yPsBIxHcjSpd3dvkZdOoouxlxyOhCgsHYvuY";

  // some help code, remember mn, pwd, lang to cookie, and autofill.
  // document.getElementById("display_name").value =
  // userName +
  //   testTool.getBrowserInfo();
  // document.getElementById("meeting_number").value =meetingNumber
  // document.getElementById("meeting_pwd").value = meetingPassword
  // if (testTool.getCookie("meeting_lang"))
  //   document.getElementById("meeting_lang").value = 'en-US';

  // document
  //   .getElementById("meeting_lang")
  //   .addEventListener("change", function (e) {
  //     testTool.setCookie(
  //       "meeting_lang",
  //      'en-US'
  //     );
  //     testTool.setCookie(
  //       "_zm_lang",
  //      'en-US'
  //     );
  //   });
  // // copy zoom invite link to mn, autofill mn and pwd.
  // document
  //   .getElementById("meeting_number")
  //   .addEventListener("input", function (e) {
  //     var tmpMn =meetingNumber
  //     if (tmpMn.match(/([0-9]{9,11})/)) {
  //       tmpMn = tmpMn.match(/([0-9]{9,11})/)[1];
  //     }
  //     var tmpPwd = meetingPassword
  //     if (tmpPwd) {
  //       document.getElementById("meeting_pwd").value =meetingPassword;
  //       testTool.setCookie("meeting_pwd",meetingPassword);
  //     }
  //     document.getElementById("meeting_number").value = tmpMn;
  //     testTool.setCookie(
  //       "meeting_number",
  //       meetingNumber
  //     )
  //   });

  // document.getElementById("clear_all").addEventListener("click", function (e) {
  //   testTool.deleteAllCookies();
  //   document.getElementById("display_name").value = "";
  //   document.getElementById("meeting_number").value = "";
  //   document.getElementById("meeting_pwd").value = "";
  //   document.getElementById("meeting_lang").value = "en-US";
  //   document.getElementById("meeting_role").value = 0;
  //   window.location.href = "/index.html";
  // });

  // click join meeting button
  document
    .getElementById("join_meeting")
    .addEventListener("click", function (e) {
      e.preventDefault();
      var meetingConfig = testTool.getMeetingConfig();
      if (!meetingConfig.mn || !meetingConfig.name) {
        alert("Meeting number or username is empty");
        return false;
      }
// alert(8888)
      
      testTool.setCookie("meeting_number", meetingConfig.mn);
      testTool.setCookie("meeting_pwd", meetingConfig.pwd);

      var signature = ZoomMtg.generateSDKSignature({
        meetingNumber: meetingConfig.mn,
        sdkKey: SDK_KEY,
        sdkSecret: SDK_SECRET,
        role: meetingConfig.role,
        success: function (res) {
          console.log(res.result);
          meetingConfig.signature = res.result;
          meetingConfig.sdkKey = SDK_KEY;
          console.log("meetingConfigmeetingConfigmeetingConfig",meetingConfig)

          var joinUrl = "/zoom/meeting.html?" + testTool.serialize(meetingConfig);
          console.log(joinUrl);
          window.open(joinUrl, "_blank");
        },
      });
    });

  function copyToClipboard(elementId) {
    var aux = document.createElement("input");
    aux.setAttribute("value", document.getElementById(elementId).getAttribute('link'));
    document.body.appendChild(aux);  
    aux.select();
    document.execCommand("copy");
    document.body.removeChild(aux);
  }
    
  // click copy jon link button
  window.copyJoinLink = function (element) {
    var meetingConfig = testTool.getMeetingConfig();
    if (!meetingConfig.mn || !meetingConfig.name) {
      alert("Meeting number or username is empty");
      return false;
    }
    var signature = ZoomMtg.generateSDKSignature({
      meetingNumber: meetingConfig.mn,
      sdkKey: SDK_KEY,
      sdkSecret: SDK_SECRET,
      role: meetingConfig.role,
      success: function (res) {
        console.log(res.result);
        meetingConfig.signature = res.result;
        meetingConfig.sdkKey = SDK_KEY;
        var joinUrl =
          testTool.getCurrentDomain() +
          "/meeting.html?" +
          testTool.serialize(meetingConfig);
        document.getElementById('copy_link_value').setAttribute('link', joinUrl);
        copyToClipboard('copy_link_value');
        
      },
    });
  };

}
